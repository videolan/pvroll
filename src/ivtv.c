/*****************************************************************************
 * ivtv.c
 *****************************************************************************
 * Copyright (C) 2010 VideoLAN
 * $Id$
 *
 * Authors: Andy Gatward <a.j.gatward@reading.ac.uk>
 *
 * ivtv_addV4L2ExtCtrl based on similar function in MPlayer stream/stream-pvr.c
 * Copyright (C) 2006 Benjamin Zores
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

#include "pvroll.h"

static int ivtv_AudioBitrate( int i_ab );

static int ivtv_doIoctl( uint32_t i_type, void *p_data )
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    int retval = ioctl( p_pvr->i_fd, i_type, p_data );
    return( retval );
}

static void ivtv_addV4L2ExtCtrl( struct v4l2_ext_control *p_ctrl, uint32_t i_id, int32_t i_value )
{
    p_ctrl->id    = i_id;
    p_ctrl->value = i_value;
}

int ivtv_Open()
{
    char sz_device[12];
    snprintf( sz_device, 12, "/dev/video%d", p_pvr->i_device );

    p_pvr->i_fd = open( sz_device, O_RDWR );

    if( p_pvr->i_fd < 0 )
    {
        msg_Err( NULL, "Failed to open %s: %s", sz_device, strerror( errno ) );
        return( -errno );
    }

    struct v4l2_capability *p_caps = malloc( sizeof( struct v4l2_capability ) );
    memset( p_caps, 0, sizeof( p_caps ) );

    if( ivtv_doIoctl( VIDIOC_QUERYCAP, p_caps ) < 0 )
    {
        msg_Err( NULL, "Could not query capabilities of device %s: %s", sz_device, strerror( errno ) );
        return( -errno );
    }

    char *psz_driver = malloc( 16 );
    snprintf( psz_driver, 16, "%s", p_caps->driver );

    if( strcmp( psz_driver, "ivtv" ) != 0 )
    {
        msg_Err( NULL, "Device %s does not appear to be an IVTV device.  Aborting.", sz_device );
        return( -EINVAL );
    }

    if( ( p_caps->capabilities & V4L2_CAP_VIDEO_CAPTURE ) == 0 )
    {
        msg_Err( NULL, "Device %s does not appear to be capable of video capture.", sz_device );
        return( -EINVAL );
    }

    snprintf( p_pvr->sz_card, 32, "%s", p_caps->card );

    msg_Dbg( NULL, "Successfully opened %s", sz_device );
    msg_Dbg( NULL, "%s card using %s driver version %u.%u.%u", p_caps->card, p_caps->driver,
             ( p_caps->version >> 16 ) & 0xff,  ( p_caps->version >> 8 ) & 0xff, p_caps->version & 0xff );

    free( p_caps );

    return( 0 );
}

void ivtv_Close()
{
    if( p_pvr->i_fd >= 0 )
    {
        close( p_pvr->i_fd );
        msg_Dbg( NULL, "Closed handle to encoder card." );
    }
}

int ivtv_Read( unsigned char *p_data, uint16_t i_len )
{
    return( read( p_pvr->i_fd, p_data, i_len ) );
}

int ivtv_InitEncoder()
{
    struct v4l2_control ctrl;

    ctrl.id = V4L2_CID_AUDIO_MUTE;
    ctrl.value = 0;

    if( ivtv_doIoctl( VIDIOC_S_CTRL, &ctrl ) < 0 )
    {
        msg_Warn( NULL, "Failed to un-mute audio: %s", strerror( errno ) );
    }

    msg_Dbg( NULL, "Audio un-muted." );

    int i_setup_rv = ivtv_SetInput();

    if( i_setup_rv < 0 )
        return( i_setup_rv );

    i_setup_rv = ivtv_SetStandard();
    
    if( i_setup_rv < 0 )
        return( i_setup_rv );

    i_setup_rv = ivtv_SetFrequency();

    if( i_setup_rv < 0 )
        return( i_setup_rv );

    // MPEG encoder controls

    struct v4l2_ext_control *p_ext_ctrl = malloc( ( IVTV_MAX_CTRLS + 5 ) * sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;
    uint8_t i_cnt = 0;

    // non-user-defined controls

    //ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_VIDEO_ENCODING, V4L2_MPEG_VIDEO_ENCODING_MPEG_2 );
    ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_AUDIO_ENCODING, V4L2_MPEG_AUDIO_ENCODING_LAYER_2 );
    ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_AUDIO_SAMPLING_FREQ, V4L2_MPEG_AUDIO_SAMPLING_FREQ_48000 );
    ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_STREAM_TYPE, V4L2_MPEG_STREAM_TYPE_MPEG2_PS );
    ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_AUDIO_MODE, V4L2_MPEG_AUDIO_MODE_STEREO );

    // user-defined controls

    ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_VIDEO_ASPECT, p_pvr->i_aspect );

    int i_abr = ivtv_AudioBitrate( p_pvr->i_audio_bitrate );
    if( i_abr >= 0 )
        ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_AUDIO_L2_BITRATE, i_abr );

    if( p_pvr->i_gop_size )
        ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_VIDEO_GOP_SIZE, p_pvr->i_gop_size );

    ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_VIDEO_BITRATE, p_pvr->i_video_bitrate );
    ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_VIDEO_BITRATE_PEAK, p_pvr->i_video_bitrate_peak );
    ivtv_addV4L2ExtCtrl( &p_ext_ctrl[i_cnt++], V4L2_CID_MPEG_VIDEO_BITRATE_MODE, p_pvr->i_video_cbr );

    // write MPEG encoder settings to card

    ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    ctrls.count      = i_cnt;
    ctrls.controls   = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_S_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error setting MPEG controls: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    free( p_ext_ctrl );

    msg_Dbg( NULL, "MPEG configuration written to card." );

    return( 0 );
}

int ivtv_GetStandard( char *psz_standard )
{
    v4l2_std_id std_id;
    struct v4l2_standard standard;

    if( ivtv_doIoctl( VIDIOC_G_STD, &std_id ) == -1 )
    {
        msg_Err( NULL, "Could not read current video standard: %s", strerror( errno ) );
        return( -errno );
    }

    memset( &standard, 0, sizeof( standard ) );
    standard.index = 0;
    
    while( ivtv_doIoctl( VIDIOC_ENUMSTD, &standard ) == 0 )
    {
        if( standard.id & std_id )
        {
            memcpy( psz_standard, &standard.name, 24 );
            return( 0 );
        }
    }

    if( errno == EINVAL || standard.index == 0 )
        msg_Warn( NULL, "Reached end of video standard enumeration without a match." );

    return( -EINVAL );
}

int ivtv_PrintStandards()
{
    struct v4l2_input input;
    struct v4l2_standard standard;

    memset( &input, 0, sizeof( input ));

    if( ivtv_doIoctl( VIDIOC_G_INPUT, &input.index ) < 0 )
    {
        msg_Err( NULL, "Failed to read index of selected input: %s", strerror( errno ) );
        return( -errno );
    }

    if( ivtv_doIoctl( VIDIOC_ENUMINPUT, &input ) < 0 )
    {
        msg_Err( NULL, "Failed to read supported video standards for input: %s", strerror( errno ) );
        return( -errno );
    }

    msg_Raw( NULL, "" );
    msg_Raw( NULL, "Selected input (%s) supports these video standards:", input.name );

    memset( &standard, 0, sizeof( standard ) );
    standard.index = 0;

    while( ivtv_doIoctl( VIDIOC_ENUMSTD, &standard ) == 0 )
    {
        if( standard.id & input.std )
            msg_Raw( NULL, "    %s", standard.name );
        standard.index++;
    }

    if( errno != EINVAL ) {
        msg_Err( NULL, "Error while enumerating supported video standards: %s", strerror( errno ) );
        return( -errno );
    }

    return( 0 );
}

int ivtv_SetStandard()
{
    struct v4l2_input input;
    memset( &input, 0, sizeof( input ) );

    if( ivtv_doIoctl( VIDIOC_G_INPUT, &input.index ) < 0 ) 
    {
        msg_Err( NULL, "Failed to read index of selected input: %s", strerror( errno ) );
        return( -errno );
    }

    if( ivtv_doIoctl( VIDIOC_ENUMINPUT, &input ) < 0 )
    {
        msg_Err( NULL, "Failed to enumerate input: %s", strerror( errno ) );
        return( -errno );
    }

    if( ( p_pvr->i_vid_std & input.std ) == 0 )
    {
        msg_Err( NULL, "Requested video standard is not supported." );
        return( -errno );
    }

    if( ivtv_doIoctl( VIDIOC_S_STD, &p_pvr->i_vid_std ) < 0 )
    {
        msg_Err( NULL, "Failed to set requested video standard: %s", strerror( errno ) );
        return( -errno );
    }

    return( 0 );
}

int ivtv_GetFrequency()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    struct v4l2_frequency vfreq;
    struct v4l2_tuner vtuner;

    memset( &vfreq, 0, sizeof( vfreq ) );
    memset( &vtuner, 0, sizeof( vtuner ) );

    if( ivtv_doIoctl( VIDIOC_G_TUNER, &vtuner ) < 0 )
    {
        msg_Warn( NULL, "Could not select tuner: %s", strerror( errno ) );
        return( -errno );
    }

    if( ivtv_doIoctl( VIDIOC_G_FREQUENCY, &vfreq ) < 0 )
    {
        msg_Warn( NULL, "Could not read tuner frequency: %s", strerror( errno ) );
        return( -errno );
    }

    if( vtuner.capability & V4L2_TUNER_CAP_LOW )
        return( vfreq.frequency / 1000 );

    return( vfreq.frequency );
}

int ivtv_SetFrequency()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    if( p_pvr->i_freq < 0 )
    {
        msg_Warn( NULL, "Frequency %d is not valid.", p_pvr->i_freq );
        return( -EINVAL );
    }

    if( p_pvr->i_freq == 0 )
        return( 0 );

    if( ivtv_GetFrequency() == p_pvr->i_freq )
    {
        msg_Dbg( NULL, "Tuner is already set to frequency %d, no action taken.", p_pvr->i_freq );
        return( 0 );
    }

    struct v4l2_frequency vfreq;
    struct v4l2_tuner vtuner;

    memset( &vfreq, 0, sizeof( vfreq ) );
    memset( &vtuner, 0, sizeof( vtuner ) );

    if( ivtv_doIoctl( VIDIOC_G_TUNER, &vtuner ) < 0 )
    {
        msg_Warn( NULL, "Could not select tuner: %s", strerror( errno ) );
        return( -errno );
    }

    vfreq.type = vtuner.type;
    vfreq.frequency = p_pvr->i_freq;

    if( vtuner.capability & V4L2_TUNER_CAP_LOW )
        vfreq.frequency *= 1000;

    if( ivtv_doIoctl( VIDIOC_S_FREQUENCY, &vfreq ) < 0 )
    {
        msg_Warn( NULL, "Could not set tuner frequency to %d: %s", p_pvr->i_freq, strerror( errno ) );
        return( -errno );
    }

    msg_Dbg( NULL, "Tuner set to frequency %d.", p_pvr->i_freq );
    memset( &vtuner, 0, sizeof( vtuner ) );

    if( ivtv_doIoctl( VIDIOC_G_TUNER, &vtuner ) < 0 )
    {
        msg_Warn( NULL, "Could not select tuner: %s", strerror( errno ) );
        return( -errno );
    }

    if( !vtuner.signal )
        msg_Dbg( NULL, "No input signal detected on frequency %d.", p_pvr->i_freq );
    else
        msg_Dbg( NULL, "Valid input signal detected on frequency %d.", p_pvr->i_freq );

    return( 0 );
}

static int ivtv_AudioBitrate( int i_ab )
{
    switch( i_ab )
    {
        case 32:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_32K );
        case 48:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_48K );
        case 56:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_56K );
        case 64:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_64K );
        case 80:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_80K );
        case 96:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_96K );
        case 112:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_112K );
        case 128:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_128K );
        case 160:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_160K );
        case 192:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_192K );
        case 224:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_224K );
        case 256:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_256K );
        case 320:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_320K );
        case 384:
            return( V4L2_MPEG_AUDIO_L2_BITRATE_384K );
        default:
            msg_Warn( NULL, "%d is not a valid audio bitrate, setting not changed.", i_ab );
            return( -EINVAL );
    }
}

int ivtv_GetAudioBw()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id   = V4L2_CID_MPEG_AUDIO_L2_BITRATE;
    ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    ctrls.count      = 1;
    ctrls.controls   = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error retrieving audio bitrate: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    int i_ab = p_ext_ctrl->value;

    free( p_ext_ctrl );

    switch( i_ab )
    {
        case V4L2_MPEG_AUDIO_L2_BITRATE_32K:
            return( 32 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_48K:
            return( 48 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_56K:
            return( 56 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_64K:
            return( 64 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_80K:
            return( 80 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_96K:
            return( 96 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_112K:
            return( 112 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_128K:
            return( 128 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_160K:
            return( 160 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_192K:
            return( 192 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_224K:
            return( 224 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_256K:
            return( 256 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_320K:
            return( 320 );
        case V4L2_MPEG_AUDIO_L2_BITRATE_384K:
            return( 384 );
    }

    return( -EINVAL );
}

int ivtv_SetAudioBw()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id = V4L2_CID_MPEG_AUDIO_L2_BITRATE;

    int i_ab = ivtv_AudioBitrate( p_pvr->i_audio_bitrate );
    if( i_ab < 0 )
        return( -EINVAL );

    ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    ctrls.count      = 1;
    ctrls.controls   = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_S_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error setting audio bitrate: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    free( p_ext_ctrl );
    return( 0 );
}

int ivtv_GetVideoAvgBw()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id   = V4L2_CID_MPEG_VIDEO_BITRATE;
    ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    ctrls.count      = 1;
    ctrls.controls   = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error retrieving video average bitrate: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    int i_vb = p_ext_ctrl->value;

    free( p_ext_ctrl );
    return( i_vb );
}

int ivtv_SetVideoAvgBw()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    if( ( p_pvr->i_video_bitrate <= 0 ) || ( p_pvr->i_video_bitrate > 27000 ) )
        return( -EINVAL );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id    = V4L2_CID_MPEG_VIDEO_BITRATE;
    p_ext_ctrl->value = p_pvr->i_video_bitrate;
    ctrls.ctrl_class  = V4L2_CTRL_CLASS_MPEG;
    ctrls.count       = 1;
    ctrls.controls    = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error setting video average bitrate: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    free( p_ext_ctrl );
    return( 0 );
}

int ivtv_GetVideoPeakBw()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id   = V4L2_CID_MPEG_VIDEO_BITRATE_PEAK;
    ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    ctrls.count      = 1;
    ctrls.controls   = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error retrieving video max bitrate: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    int i_vb = p_ext_ctrl->value;

    free( p_ext_ctrl );
    return( i_vb );
}

int ivtv_SetVideoPeakBw()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    if( ( p_pvr->i_video_bitrate_peak <= 0 ) || ( p_pvr->i_video_bitrate_peak > 27000 ) )
        return( -EINVAL );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id    = V4L2_CID_MPEG_VIDEO_BITRATE_PEAK;
    p_ext_ctrl->value = p_pvr->i_video_bitrate_peak;
    ctrls.ctrl_class  = V4L2_CTRL_CLASS_MPEG;
    ctrls.count       = 1;
    ctrls.controls    = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error setting video max bitrate: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    free( p_ext_ctrl );
    return( 0 );
}

int ivtv_GetCbrMode()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id   = V4L2_CID_MPEG_VIDEO_BITRATE_PEAK;
    ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    ctrls.count      = 1;
    ctrls.controls   = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error retrieving video max bitrate: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    int i_cbr = p_ext_ctrl->value;

    free( p_ext_ctrl );
    return( i_cbr );
}

int ivtv_SetCbrMode()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    if( ( p_pvr->i_video_cbr != V4L2_MPEG_VIDEO_BITRATE_MODE_VBR ) &&
        ( p_pvr->i_video_cbr != V4L2_MPEG_VIDEO_BITRATE_MODE_CBR ) )
        return( -EINVAL );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id    = V4L2_CID_MPEG_VIDEO_BITRATE_MODE;
    p_ext_ctrl->value = p_pvr->i_video_cbr;
    ctrls.ctrl_class  = V4L2_CTRL_CLASS_MPEG;
    ctrls.count       = 1;
    ctrls.controls    = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error setting video CBR mode: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    free( p_ext_ctrl );
    return( 0 );
}

int ivtv_GetGopSize()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id   = V4L2_CID_MPEG_VIDEO_GOP_SIZE;
    ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    ctrls.count      = 1;
    ctrls.controls   = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error retrieving GOP size: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    int i_gop_size = p_ext_ctrl->value;

    free( p_ext_ctrl );
    return( i_gop_size );
}

int ivtv_SetGopSize()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    if( ( p_pvr->i_gop_size <= 0 ) || ( p_pvr->i_gop_size > 18 ) )
        return( -EINVAL );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id    = V4L2_CID_MPEG_VIDEO_GOP_SIZE;
    p_ext_ctrl->value = p_pvr->i_gop_size;
    ctrls.ctrl_class  = V4L2_CTRL_CLASS_MPEG;
    ctrls.count       = 1;
    ctrls.controls    = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error setting GOP size: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    free( p_ext_ctrl );
    return( 0 );
}

void ivtv_PrintInputList()
{
    if( p_pvr->i_fd >= 0 )
    {
        msg_Raw( NULL, "" );
        msg_Raw( NULL, "Video inputs available on this card (%s) are:", p_pvr->sz_card );

        int i_idx = 0;
        struct v4l2_input *p_input = malloc( sizeof( struct v4l2_input ) );
        memset( p_input, 0, sizeof( p_input ) );
        p_input->index = i_idx;

        while( ivtv_doIoctl( VIDIOC_ENUMINPUT, p_input ) >= 0 )
        {
            msg_Raw( NULL, "%*d: %s", 5, i_idx, p_input->name );
            memset( p_input, 0, sizeof( p_input ) );
            p_input->index = ++i_idx;
        }

        free( p_input );
    }
}

int ivtv_GetInput()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    int i_input;

    if( ivtv_doIoctl( VIDIOC_G_INPUT, &i_input ) < 0 )
    {
        msg_Err( NULL, "Could not read current input: %s", strerror( errno ) );
        return( -errno );
    }

    return( i_input );
}

int ivtv_SetInput()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    if( ivtv_doIoctl( VIDIOC_S_INPUT, &p_pvr->i_input ) < 0 )
    {
        msg_Err( NULL, "Could not set input to %d: %s", p_pvr->i_input, strerror( errno ) );
        return( -errno );
    }

    msg_Dbg( NULL, "Encoder card input %d selected.", p_pvr->i_input );

    return( 0 );
}

int ivtv_GetAspect()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id   = V4L2_CID_MPEG_VIDEO_ASPECT;
    ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    ctrls.count      = 1;
    ctrls.controls   = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error retrieving aspect ratio: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    int i_aspect = p_ext_ctrl->value;

    free( p_ext_ctrl );
    return( i_aspect );
}

int ivtv_SetAspect()
{
    if( p_pvr->i_fd < 0 )
        return( -EBADFD );

    if( ( p_pvr->i_aspect != V4L2_MPEG_VIDEO_ASPECT_1x1 ) &&
        ( p_pvr->i_aspect != V4L2_MPEG_VIDEO_ASPECT_4x3 ) &&
        ( p_pvr->i_aspect != V4L2_MPEG_VIDEO_ASPECT_16x9 ) &&
        ( p_pvr->i_aspect != V4L2_MPEG_VIDEO_ASPECT_221x100 ) )
        return( -EINVAL );

    struct v4l2_ext_control *p_ext_ctrl = malloc( sizeof( struct v4l2_ext_control ) );
    struct v4l2_ext_controls ctrls;

    memset( p_ext_ctrl, 0, sizeof( p_ext_ctrl ) );
    p_ext_ctrl->id    = V4L2_CID_MPEG_VIDEO_ASPECT;
    p_ext_ctrl->value = p_pvr->i_gop_size;
    ctrls.ctrl_class  = V4L2_CTRL_CLASS_MPEG;
    ctrls.count       = 1;
    ctrls.controls    = p_ext_ctrl;

    if( ivtv_doIoctl( VIDIOC_G_EXT_CTRLS, &ctrls ) < 0 )
    {
        msg_Err( NULL, "Error setting aspect ratio: %s", strerror( errno ) );
        free( p_ext_ctrl );
        return( -errno );
    }

    free( p_ext_ctrl );
    return( 0 );
}
