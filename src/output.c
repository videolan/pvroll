/*****************************************************************************
 * output.c
 *****************************************************************************
 * Copyright (C) 2004, 2008-2010 VideoLAN
 * $Id$
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *          Andy Gatward <a.j.gatward@reading.ac.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <errno.h>

#include "pvroll.h"

/*****************************************************************************
 * Local prototypes
 *****************************************************************************/
static void rtp_SetHdr( uint8_t *p_hdr );

/*****************************************************************************
 * output_Flush
 *****************************************************************************/
static void output_Flush()
{
    struct iovec p_iov[( p_output->i_rtp_depth + 1 )];
    uint8_t i_rtp_hdr[RTP_SIZE];
    uint8_t i;

    if( !( p_output->i_flags & OUTPUT_UDP ) )
    {
        p_iov[0].iov_base = i_rtp_hdr;
        p_iov[0].iov_len  = sizeof( i_rtp_hdr );
        rtp_SetHdr( i_rtp_hdr );

        for( i = 0; i < p_output->i_rtp_depth; i++ )
        {
            p_iov[i + 1].iov_base = p_output->pp_blocks[i]->p_ts;
            p_iov[i + 1].iov_len  = TS_SIZE;
        }

        if ( writev( p_output->i_fd, p_iov, ( p_output->i_rtp_depth + 1 ) ) < 0 )
            msg_Err( NULL, "Couldn't write to output: %s", strerror( errno ) );
    }
    else
    {
        for( i = 0; i < p_output->i_rtp_depth; i++ )
        {
            p_iov[i].iov_base = p_output->pp_blocks[i]->p_ts;
            p_iov[i].iov_len  = TS_SIZE;
        }

        if ( writev( p_output->i_fd, p_iov, ( p_output->i_rtp_depth ) ) < 0 )
            msg_Err( NULL, "Couldn't write to output: %s", strerror( errno ) );
    }

    for( i = 0; i < p_output->i_rtp_depth; i++ )
        free( p_output->pp_blocks[i] );

    p_output->i_rtp_pos = 0;
}

/*****************************************************************************
 * output_PutBlock
 *****************************************************************************/
void output_PutBlock( block_t *p_block )
{
    p_output->pp_blocks[p_output->i_rtp_pos++] = p_block;
    if( p_output->i_rtp_pos == p_output->i_rtp_depth )
        output_Flush();
}

/*****************************************************************************
 * output_Open
 *****************************************************************************/
int output_Open()
{
    p_output->i_fd = socket( p_output->p_addr->ss_family, SOCK_DGRAM, IPPROTO_UDP );

    if( p_output->i_fd < 0 )
    {
        msg_Err( NULL, "Couldn't create socket: %s", strerror( errno ) );
        return -errno;
    }

    if( p_output->p_addr->ss_family == AF_INET6 )
    {
        struct sockaddr_in6 *addr = (struct sockaddr_in6 *)p_output->p_addr;
        if( IN6_IS_ADDR_MULTICAST( addr->sin6_addr.s6_addr ) )
        {
            int i = p_output->i_ttl;
            setsockopt( p_output->i_fd, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                        (void *)&i, sizeof(i) );
        }
    }

    else if( p_output->p_addr->ss_family == AF_INET )
    {
        struct sockaddr_in *addr = (struct sockaddr_in *)p_output->p_addr;
        if( IN_MULTICAST( ntohl( addr->sin_addr.s_addr ) ) )
        {
            int i = p_output->i_ttl;
            setsockopt( p_output->i_fd, IPPROTO_IP, IP_MULTICAST_TTL,
                        (void *)&i, sizeof(i) );
        }
    }

    if( connect( p_output->i_fd, (struct sockaddr *)p_output->p_addr, p_output->i_addrlen ) < 0 )
    {
        msg_Err( NULL, "Couldn't connect socket: %s", strerror( errno ) );
        close( p_output->i_fd );
        return -errno;
    }

    return( 0 );
}

/*****************************************************************************
 * output_Close
 *****************************************************************************/
void output_Close()
{
    if( p_output->i_fd >= 0 )
    {
        close( p_output->i_fd );
        p_output->i_fd = -1;
    }
}

/*****************************************************************************
 * output_Init()
 *****************************************************************************/
void output_Init()
{
    p_output->i_rtp_seq    = rand() & 0xffff;
    p_output->i_rtp_ssrc   = rand() & 0xffffffff;
    p_output->i_rtp_tstamp = 0;
    p_output->i_rtp_wclock = 0;
    if( !( p_output->i_flags & OUTPUT_UDP ) )
        p_output->i_rtp_depth = ( p_output->i_mtu - RTP_SIZE ) / TS_SIZE;
    else
        p_output->i_rtp_depth = p_output->i_mtu / TS_SIZE;
    p_output->i_pcr        = 0;
    p_output->b_send_pcr   = 0;
    p_output->i_cc_video   = rand() & 0x0f;
    p_output->i_cc_audio   = rand() & 0x0f;
    p_output->i_cc_pat     = rand() & 0x0f;
    p_output->i_cc_pmt     = rand() & 0x0f;
    p_output->i_pat_ver    = rand() & 0x1f;
    p_output->i_pmt_ver    = rand() & 0x1f;
}

/*****************************************************************************
 * rtp_SetHdr
 *****************************************************************************/
/*
 * Reminder : RTP header
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |V=2|P|X|  CC   |M|     PT      |       sequence number         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                           timestamp                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |           synchronization source (SSRC) identifier            |
   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
   |            contributing source (CSRC) identifiers             |
   |                             ....                              |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

static void rtp_SetHdr( uint8_t *p_hdr )
{
    mtime_t i_timestamp;

    if (!p_output->i_rtp_wclock)
    {
        i_timestamp = p_output->i_rtp_tstamp;
        p_output->i_rtp_wclock = mdate();
    }
    else
    {
        i_timestamp = p_output->i_rtp_tstamp
                    + (mdate() - p_output->i_rtp_wclock) * 9 / 100;
    }

    p_hdr[0] = 0x80;
    p_hdr[1] = 33;
    p_hdr[2] = p_output->i_rtp_seq >> 8;
    p_hdr[3] = p_output->i_rtp_seq & 0xff;
    p_hdr[4] = (i_timestamp >> 24) & 0xff;
    p_hdr[5] = (i_timestamp >> 16) & 0xff;
    p_hdr[6] = (i_timestamp >> 8) & 0xff;
    p_hdr[7] = i_timestamp & 0xff;
    p_hdr[8] = ((uint8_t *)&p_output->i_rtp_ssrc)[0];
    p_hdr[9] = ((uint8_t *)&p_output->i_rtp_ssrc)[1];
    p_hdr[10] = ((uint8_t *)&p_output->i_rtp_ssrc)[2];
    p_hdr[11] = ((uint8_t *)&p_output->i_rtp_ssrc)[3];
    p_output->i_rtp_seq++;
}

