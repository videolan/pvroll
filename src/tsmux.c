/*****************************************************************************
 * mpeg.c
 *****************************************************************************
 * Copyright (C) 2010 VideoLAN
 * $Id$
 *
 * Authors: Andy Gatward <a.j.gatward@reading.ac.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "pvroll.h"

#include <dvbpsi/descriptor.h>
#include <dvbpsi/pmt.h>
#include <dvbpsi/pat.h>
#include <dvbpsi/psi.h>

#define TSMUX_STATE_START_0      0   // looking for start code byte 0
#define TSMUX_STATE_START_1      1   // looking for start code byte 1
#define TSMUX_STATE_START_2      2   // looking for start code byte 2
#define TSMUX_STATE_START_3      3   // determining header type
#define TSMUX_STATE_IN_PACK      4   // inside PS pack header
#define TSMUX_STATE_IN_SYS       5   // inside system header

#define TSMUX_HEADER_PACK        0xBA
#define TSMUX_HEADER_SYS         0xBB
#define TSMUX_PES_BASE           0xBC
#define TSMUX_PES_PRIVATE_1      0xBD
#define TSMUX_PES_PADDING        0xBE
#define TSMUX_PES_PRIVATE_2      0xBF
#define TSMUX_PES_VID_START      0xE0
#define TSMUX_PES_VID_END        0xEF
#define TSMUX_PES_AUD_START      0xC0
#define TSMUX_PES_AUD_END        0xDF

static int tsmux_ReadBytes( int i_bytes )
{
    int i_br, i_rv;
    i_br = i_rv = 0;
    while( i_br < i_bytes )
    {
        i_rv = ivtv_Read( p_output->p_psdata, i_bytes - i_br );
        if( i_rv < 0 )
            return( i_rv );
        i_br += i_rv;
    }
    return( i_br );
}

static int tsmux_PushESData( uint16_t i_pid, uint8_t i_cc, uint8_t *p_data, uint16_t i_size )
{
    uint16_t idx = 0;
    uint8_t i_payload_size;

    while( idx < i_size )
    {
        block_t *p_block;
        p_block = malloc( sizeof( block_t ) );
        memset( p_block, 0xff, sizeof( block_t ) );
        i_payload_size = TS_PAYLOAD_SIZE;

        p_block->p_ts[0] = 0x47;
        p_block->p_ts[1] = idx == 0 ? 0x40 : 0x00 | ( (uint8_t)( i_pid >> 8 ) & 0x1f );
        p_block->p_ts[2] = (uint8_t)( i_pid & 0x00ff );
        p_block->p_ts[3] = 0x10 | ( i_cc++ & 0x0f );

        // if PCR is due to be sent, insert in video PID

        if( ( i_pid == p_output->i_pid_video ) && p_output->b_send_pcr )
        {
            i_payload_size -= 8;

            p_block->p_ts[3] |= 0x20;
            p_block->p_ts[4]  = 7;
            p_block->p_ts[5]  = 0x10;
            p_block->p_ts[6]  = (uint8_t)( ( p_output->i_pcr >> 34 ) & 0xff );
            p_block->p_ts[7]  = (uint8_t)( ( p_output->i_pcr >> 26 ) & 0xff );
            p_block->p_ts[8]  = (uint8_t)( ( p_output->i_pcr >> 18 ) & 0xff );
            p_block->p_ts[9]  = (uint8_t)( ( p_output->i_pcr >> 10 ) & 0xff );
            p_block->p_ts[10] = (uint8_t)( ( p_output->i_pcr >>  2 ) & 0x80 ) |
                0x7e | (uint8_t)( ( p_output->i_pcr >>  8 ) & 0x01 );
            p_block->p_ts[11] = (uint8_t)( ( p_output->i_pcr ) & 0xff );

            p_output->b_send_pcr = 0;
        }

        // if we are at the start of an ES header, audio and video PID packets need
        // marking as random access to avoid sync problems.

        if( ( idx == 0 ) &&
            ( i_pid == p_output->i_pid_audio || i_pid == p_output->i_pid_video ) )
        {
            if( p_block->p_ts[3] & 0x20 )
                p_block->p_ts[5] |= 0x40;
            else
            {
                i_payload_size -= 2;
                p_block->p_ts[3] |= 0x20;
                p_block->p_ts[4]  = 1;
                p_block->p_ts[5]  = 0x40;
            }
        }

        // insert payload

        if( ( idx + i_payload_size ) > i_size )
        {
            uint8_t i_remain = i_size - idx;

            if( p_block->p_ts[3] & 0x20 )
                p_block->p_ts[4] += ( i_payload_size - i_remain );
            else
            {
                p_block->p_ts[3] |= 0x20;
                p_block->p_ts[4] = i_payload_size - i_remain - 1;
                p_block->p_ts[5] = 0x00;
            }
            memcpy( &p_block->p_ts[TS_SIZE - i_remain], &p_data[idx], i_remain );
        }
        else
        {
            memcpy( &p_block->p_ts[TS_SIZE - i_payload_size], &p_data[idx], i_payload_size );
        }

        idx += i_payload_size;
        output_PutBlock( p_block );
    }

    return( i_cc );
}

static int tsmux_PushSection( uint16_t i_pid, uint8_t i_cc, dvbpsi_psi_section_t *p_section )
{
    while( p_section )
    {
        uint16_t i_size = (uint16_t)( p_section->i_length ) + 3;
        uint16_t idx = 0;

        block_t *p_block;
        p_block = malloc( sizeof( block_t ) );
        memset( p_block->p_ts, 0xff, TS_SIZE );

        p_block->p_ts[0] = 0x47;
        p_block->p_ts[1] = 0x40 | ( (uint8_t)( i_pid >> 8 ) & 0x1f );
        p_block->p_ts[2] = (uint8_t)( i_pid & 0x00ff );
        p_block->p_ts[3] = 0x10 | ( i_cc++ & 0x0f );
        p_block->p_ts[4] = 0x00;

        if( i_size <= TS_PAYLOAD_SIZE - 5 )
        {
            memcpy( &p_block->p_ts[5], p_section->p_data, i_size );
            output_PutBlock( p_block );
        }
        else
        {
            memcpy( &p_block->p_ts[5], p_section->p_data, ( TS_PAYLOAD_SIZE - 5 ) );
            output_PutBlock( p_block );

            idx += ( TS_PAYLOAD_SIZE - 5 );

            while( idx < i_size )
            {
                block_t *p_next = malloc( sizeof( block_t ) );
                memset( p_next->p_ts, 0xff, TS_SIZE );

                p_next->p_ts[0] = 0x47;
                p_next->p_ts[1] = ( (uint8_t)( i_pid >> 8 ) & 0x1f );
                p_next->p_ts[2] = (uint8_t)( i_pid & 0x00ff );
                p_next->p_ts[3] = 0x10 | ( i_cc++ & 0x0f );

                if( i_size - idx <= TS_PAYLOAD_SIZE )
                    memcpy( &p_next->p_ts[5], &p_section->p_data[idx], i_size - idx );
                else
                    memcpy( &p_next->p_ts[5], &p_section->p_data[idx], TS_PAYLOAD_SIZE );

                output_PutBlock( p_next );

                idx += TS_PAYLOAD_SIZE;
            }
        }

        p_section = p_section->p_next;
    }

    return( i_cc );
}

/* Reminder of pack header format (excluding start code)
 *
 * 01BBB1BB BBBBBBBB BBBBB1BB BBBBBBBB BBBBB1XX
 * XXXXXXX1 MMMMMMMM MMMMMMMM MMMMMM11 RRRRRSSS
 */

static void tsmux_ProcessPackHeader()
{
    tsmux_ReadBytes( 10 );

    p_output->i_pcr  = (( (uint64_t)p_output->p_psdata[0] & 0x38 ) << 36 );
    p_output->i_pcr |= (( (uint64_t)p_output->p_psdata[0] & 0x03 ) << 37 );
    p_output->i_pcr |= ( (uint64_t)p_output->p_psdata[1] << 29 );
    p_output->i_pcr |= (( (uint64_t)p_output->p_psdata[2] & 0xf8 ) << 21 );
    p_output->i_pcr |= (( (uint64_t)p_output->p_psdata[2] & 0x03 ) << 22 );
    p_output->i_pcr |= ( (uint64_t)p_output->p_psdata[3] << 14 );
    p_output->i_pcr |= (( (uint64_t)p_output->p_psdata[4] & 0xf8 ) << 6 );
    p_output->i_pcr |= (( (uint64_t)p_output->p_psdata[4] & 0x03 ) << 7 );
    p_output->i_pcr |= (( (uint64_t)p_output->p_psdata[5] & 0xfe ) >> 1 );

    // shift PCR back by 100ms (((27000000 / 300) * 0.1) << 9 )
    // this compensates for packetisation and most transport delays

    p_output->i_pcr -= 4608000;

    p_output->b_send_pcr = 1;

    uint8_t i_stuffing = p_output->p_psdata[9] & 0x07;
    if( i_stuffing > 0 )
        tsmux_ReadBytes( i_stuffing );
}

static void tsmux_ProcessSysHeader()
{
    tsmux_ReadBytes( 2 );
    uint16_t i_hdr_size = (uint16_t)(p_output->p_psdata[0] << 8);
    i_hdr_size         |= (uint16_t)p_output->p_psdata[1];
    tsmux_ReadBytes( i_hdr_size );

    int i_descriptors = ( i_hdr_size - 6 ) / 3;
    uint32_t i_buf_size;

    int i, idx;
    for( i = 0; i < i_descriptors; i++ )
    {
        idx = (3 * i) + 6;
        if( ( p_output->p_psdata[idx] == TSMUX_PES_PRIVATE_1 ) ||
            ( p_output->p_psdata[idx] == TSMUX_PES_PRIVATE_2 ) )
            continue;

        i_buf_size  = p_output->p_psdata[idx + 2] & 0xff;
        i_buf_size |= (uint32_t)(p_output->p_psdata[idx + 1] & 0x3f) << 8;
        if( !( p_output->p_psdata[idx + 1] & 0x40 ) )
            i_buf_size *= 128;
        else
            i_buf_size *= 1024;

        if( p_output->i_psdata_len < i_buf_size )
        {
            msg_Dbg( NULL, "Extending PS buffer to %lu bytes.", i_buf_size );
            p_output->i_psdata_len = i_buf_size;
            p_output->p_psdata = realloc( p_output->p_psdata, p_output->i_psdata_len );
        }
    }

    p_output->i_cc_pat = tsmux_PushSection( 0, p_output->i_cc_pat, p_output->p_pat_section );
    p_output->i_cc_pmt = tsmux_PushSection( p_output->i_pid_pmt, p_output->i_cc_pmt, p_output->p_pmt_section );

}


static void tsmux_ProcessPESPacket()
{
    uint8_t i_pes_type = p_output->p_psdata[0];

    tsmux_ReadBytes( 2 );
    uint16_t i_size  = (uint16_t)(p_output->p_psdata[0] << 8);
    i_size          |= (uint16_t)p_output->p_psdata[1];
    uint8_t *p_data = malloc( i_size + 6);
    p_data[0] = p_data[1] = 0;
    p_data[2] = 1;
    p_data[3] = i_pes_type;
    p_data[4] = p_output->p_psdata[0];
    p_data[5] = p_output->p_psdata[1];
    tsmux_ReadBytes( i_size );
    memcpy( &p_data[6], p_output->p_psdata, i_size );

    if( i_pes_type >= TSMUX_PES_VID_START && i_pes_type <= TSMUX_PES_VID_END )
        p_output->i_cc_video = tsmux_PushESData( p_output->i_pid_video, p_output->i_cc_video, p_data, i_size + 6 );

    else if( i_pes_type >= TSMUX_PES_AUD_START && i_pes_type <= TSMUX_PES_AUD_END )
        p_output->i_cc_audio = tsmux_PushESData( p_output->i_pid_audio, p_output->i_cc_audio, p_data, i_size + 6 );

    free( p_data );
}

static void tsmux_InitTables()
{
    struct dvbpsi_pat_s *p_pat = malloc( sizeof( struct dvbpsi_pat_s ) );
    dvbpsi_InitPAT( p_pat, p_output->i_tsid, p_output->i_pat_ver, 1 );
    dvbpsi_PATAddProgram( p_pat, p_output->i_prognum, p_output->i_pid_pmt );
    p_output->p_pat_section = dvbpsi_GenPATSections( p_pat, 1 );
    dvbpsi_EmptyPAT( p_pat );

    struct dvbpsi_pmt_es_s *p_es;
    struct dvbpsi_pmt_s *p_pmt = malloc( sizeof( struct dvbpsi_pmt_s ) );
    dvbpsi_InitPMT( p_pmt, p_output->i_prognum, p_output->i_pmt_ver, 1, p_output->i_pid_video );
    p_es = dvbpsi_PMTAddES( p_pmt, 0x02, p_output->i_pid_video );
    p_es = dvbpsi_PMTAddES( p_pmt, 0x03, p_output->i_pid_audio );
    p_output->p_pmt_section = dvbpsi_GenPMTSections( p_pmt );
    dvbpsi_EmptyPMT( p_pmt );
}

void tsmux_Run()
{
    tsmux_InitTables();

    while( i_running )
    {
        switch( p_output->i_tsmux_state )
        {
        case TSMUX_STATE_START_0:
        case TSMUX_STATE_START_1:
            tsmux_ReadBytes( 1 );
            if( p_output->p_psdata[0] == 0x00 )
                p_output->i_tsmux_state++;
            else
                p_output->i_tsmux_state = TSMUX_STATE_START_0;
            break;
        case TSMUX_STATE_START_2:
            tsmux_ReadBytes( 1 );
            if( p_output->p_psdata[0] == 0x01 )
                p_output->i_tsmux_state = TSMUX_STATE_START_3;
            else
                p_output->i_tsmux_state = TSMUX_STATE_START_0;
            break;
        case TSMUX_STATE_START_3:
            tsmux_ReadBytes( 1 );
            if( p_output->p_psdata[0] == TSMUX_HEADER_PACK )
                tsmux_ProcessPackHeader();
            else if( p_output->p_psdata[0] == TSMUX_HEADER_SYS )
                tsmux_ProcessSysHeader();
            else if( p_output->p_psdata[0] >= 0xbc )
                tsmux_ProcessPESPacket();
            break;
        default:
            p_output->i_tsmux_state = TSMUX_STATE_START_0;
        }
    }
}
