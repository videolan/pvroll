/*****************************************************************************
 * pvroll.c
 *****************************************************************************
 * Copyright (C) 2010 VideoLAN
 * $Id$
 *
 * Authors: Andy Gatward <a.j.gatward@reading.ac.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <pthread.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <getopt.h>
#include <errno.h>
#include <linux/videodev2.h>

#include "pvroll.h"
#include "version.h"

/*****************************************************************************
 * Default assignments to globals
 *****************************************************************************/

uint8_t i_verbose = DEFAULT_VERBOSE;
uint8_t i_running = 0;

/*****************************************************************************
 * Display version banner
 *****************************************************************************/

void version()
{
    msg_Raw( NULL, "PVRoll version %d.%d.%d%s", VERSION_MAJOR, VERSION_MINOR, VERSION_REVISION, VERSION_EXTRA );
}

/*****************************************************************************
 * Display usage
 *****************************************************************************/

void usage()
{
    msg_Raw( NULL, "" );
    msg_Raw( NULL, "Usage: pvroll [OPTIONS] <dest_ip>[:port]" );
    msg_Raw( NULL, "Stream MPEG data from IVTV PVR card to the network." );
    msg_Raw( NULL, "Destination can be IPv4 or IPv6 address.  Default port is %d.", DEFAULT_PORT );
    msg_Raw( NULL, "" );
    msg_Raw( NULL, "Options:" );
    msg_Raw( NULL, "  -a  Set audio bandwidth in kb/s, default %d.  Valid values are: ", DEFAULT_AB );
    msg_Raw( NULL, "          192, 224, 256, 320, 384" );
    msg_Raw( NULL, "  -b  Set video bandwidth in kb/s, default %d.", DEFAULT_VB_AVG );
    msg_Raw( NULL, "  -c  Enable CBR mode for video.  Use with -b to set bandwidth." );
    msg_Raw( NULL, "  -d  Select IVTV device to use, default %s.", DEFAULT_DEVICE );
    msg_Raw( NULL, "  -f  Tune to specified frequency (in kHz)." );
    msg_Raw( NULL, "  -g  Set MPEG2 group-of-pictures size. ");
    msg_Raw( NULL, "  -h  Show this help message." );
    msg_Raw( NULL, "  -i  Select card input." );
    msg_Raw( NULL, "  -I  List card inputs." );
    msg_Raw( NULL, "  -m  Override network MTU size, using specified value." );
    msg_Raw( NULL, "  -p  Set peak video bandwidth (VBR) in kb/s, default %d.", DEFAULT_VB_MAX );
    msg_Raw( NULL, "  -s  Force video standard (PAL, NTSC, SECAM), default auto" );
    msg_Raw( NULL, "  -S  Show supported video standards for selected input." );
    msg_Raw( NULL, "  -r  Set aspect ratio, default is 4:3, valid values are: " );
    msg_Raw( NULL, "          1:1, 4:3, 16:9, 2.21:1" );
    msg_Raw( NULL, "  -t  Set TTL for multicast output, default %d.", DEFAULT_TTL );
    msg_Raw( NULL, "  -v  Verbose output, repeat for more verbosity" );
    msg_Raw( NULL, "  -V  Display version information." );
}

/*****************************************************************************
 * Entry point
 *****************************************************************************/

int main( int i_argc, char *p_argv[] )
{
    version();

    // build PVR structure and populate with default data

    p_pvr = malloc( sizeof( struct pvr_t ) );
    memset( p_pvr, 0, sizeof( p_pvr ) );

    p_pvr->i_device             = DEFAULT_DEVICE;
    p_pvr->i_audio_bitrate      = DEFAULT_AB;
    p_pvr->i_video_bitrate      = DEFAULT_VB_AVG * 1024;
    p_pvr->i_video_bitrate_peak = DEFAULT_VB_MAX * 1024;
    p_pvr->i_video_cbr          = V4L2_MPEG_VIDEO_BITRATE_MODE_VBR;
    p_pvr->i_freq               = 0;
    p_pvr->i_gop_size           = 0;
    p_pvr->i_input              = DEFAULT_INPUT;
    p_pvr->i_aspect             = V4L2_MPEG_VIDEO_ASPECT_4x3;
    p_pvr->i_vid_std            = V4L2_STD_ALL;

    // build output structure and populate with default data

    p_output = malloc( sizeof( struct output_t ) );
    memset( p_output, 0, sizeof( p_output ) );

    p_output->i_mtu = 0;            // we use 0 to indicate default
    p_output->i_ttl = DEFAULT_TTL;
    p_output->i_pid_video = DEFAULT_PID_VIDEO;
    p_output->i_pid_audio = DEFAULT_PID_AUDIO;
    p_output->i_pid_pmt   = DEFAULT_SID;
    p_output->i_prognum   = DEFAULT_SID;
    p_output->i_tsid      = DEFAULT_TSID;

    // option parsing via getopt (long mode supported)

    static const struct option long_options[] =
    {
        { "audio-bw",    required_argument, NULL, 'a' },
        { "video-bw",    required_argument, NULL, 'b' },
        { "cbr",         no_argument,       NULL, 'c' },
        { "device",      required_argument, NULL, 'd' },
        { "frequency",   required_argument, NULL, 'f' },
        { "gop-size",    required_argument, NULL, 'g' },
        { "help",        no_argument,       NULL, 'h' },
        { "input",       required_argument, NULL, 'i' },
        { "list-inputs", no_argument,       NULL, 'I' },
        { "mtu",         required_argument, NULL, 'm' },
        { "video-maxbw", required_argument, NULL, 'p' },
        { "aspect",      required_argument, NULL, 'r' },
        { "standard",    required_argument, NULL, 's' },
        { "list-std",    no_argument,       NULL, 'S' },
        { "ttl",         required_argument, NULL, 't' },
        { "udp",         no_argument,       NULL, 'u' },
        { "version",     no_argument,       NULL, 'V' },
        { 0, 0, 0, 0 }
    };

    uint8_t b_lsinput = 0;
    uint8_t b_lsstds  = 0;
    int c;

    while( ( c = getopt_long(i_argc, p_argv, "v::a:b:cd:f:g:hi:Im:p:r:s:St:uV", long_options, NULL ) ) != -1 )
    {
        switch( c )
        {
        case 'v':
            if( optarg )
            {
                if( *optarg == 'v' )    // e.g. -vvv
                {
                    i_verbose++;
                    while( *optarg == 'v' )
                    {
                        i_verbose++;
                        optarg++;
                    }
                }
                else
                {
                    i_verbose += atoi( optarg );    // e.g. -v2
                }
            }
            else
            {
                i_verbose++;    // -v
            }
            break;
        case 'a':
            p_pvr->i_audio_bitrate = strtol( optarg, NULL, 10 );
            if( p_pvr->i_audio_bitrate < 32 || p_pvr->i_audio_bitrate > 384 )
            {
                msg_Warn( NULL, "'%s' is not a valid audio bitrate, using default.", optarg );
                p_pvr->i_audio_bitrate = DEFAULT_AB;
            }
            break;
        case 'b':
            p_pvr->i_video_bitrate = strtol( optarg, NULL, 10 );
            if( p_pvr->i_video_bitrate <= 0 || p_pvr->i_video_bitrate >= 27000 )
            {
                msg_Warn( NULL, "'%s' is not a valid video bitrate, using default.", optarg );
                p_pvr->i_video_bitrate = DEFAULT_VB_AVG;
            }
            break;
        case 'c':
            p_pvr->i_video_cbr = V4L2_MPEG_VIDEO_BITRATE_MODE_CBR;
            break;
        case 'd':
            p_pvr->i_device = strtol( optarg, NULL, 10 );
            if(( p_pvr->i_device == 0 && errno == EINVAL ) || p_pvr->i_device > 11 )
            {
                msg_Err( NULL, "'%s' is not a valid card number.", optarg );
                exit( EXIT_FAILURE );
            }
            break;
        case 'f':
            p_pvr->i_freq = strtol( optarg, NULL, 10 );
            if( ( p_pvr->i_freq == 0 && errno == EINVAL ) ||
                ( p_pvr->i_freq < 50000 ) || ( p_pvr->i_freq > 860000 ) )
            {
                msg_Err( NULL, "'%s' is not a valid frequency.", optarg );
                exit( EXIT_FAILURE );
            }
            break;
        case 'g':
            p_pvr->i_gop_size = strtol( optarg, NULL, 10 );
            if( p_pvr->i_gop_size == 0 || p_pvr->i_gop_size > 18 )
            {
                msg_Warn( NULL, "'%s' is not a valid GOP size, using default.", optarg );
                p_pvr->i_gop_size = 0;
            }
            break;
        case 'h':
            usage();
            exit( 0 );
            break;
        case 'i':
            p_pvr->i_input = strtol( optarg, NULL, 10 );
            if( p_pvr->i_input == 0 && errno == EINVAL )
            {
                msg_Err( NULL, "'%s' is not a valid input selection.", optarg );
                exit( EXIT_FAILURE );
            }
            break;
        case 'I':
            b_lsinput = 1;
            break;
        case 'm':
            p_output->i_mtu = strtol( optarg, NULL, 10 );
            if( p_output->i_mtu < MIN_MTU || p_output->i_mtu > MAX_MTU )
            {
                msg_Warn( NULL, "'%s' is not a valid MTU, using default.", optarg );
                p_output->i_mtu = 0;
            }
            break;
        case 'p':
            p_pvr->i_video_bitrate_peak = strtol( optarg, NULL, 10 );
            if( p_pvr->i_video_bitrate_peak <= 0 || p_pvr->i_video_bitrate_peak >= 27000 )
            {
                msg_Warn( NULL, "'%s' is not a valid video bitrate, using default.", optarg );
                p_pvr->i_video_bitrate_peak = DEFAULT_VB_MAX;
            }
            break;
        case 'r':
            if( strcmp( optarg, "1:1" ) == 0 )
                p_pvr->i_aspect = V4L2_MPEG_VIDEO_ASPECT_1x1;
            else if( strcmp( optarg, "4:3" ) == 0 )
                p_pvr->i_aspect = V4L2_MPEG_VIDEO_ASPECT_4x3;
            else if( strcmp( optarg, "16:9" ) == 0 )
                p_pvr->i_aspect = V4L2_MPEG_VIDEO_ASPECT_16x9;
            else if( strcmp( optarg, "2.21:1" ) == 0 )
                p_pvr->i_aspect = V4L2_MPEG_VIDEO_ASPECT_221x100;
            else 
            {
                msg_Warn( NULL, "'%s' is not a valid aspect ratio, using default.", optarg );
                p_pvr->i_aspect = V4L2_MPEG_VIDEO_ASPECT_4x3;
            }
            break;
        case 's':
            if( strcmp( optarg, "NTSC" ) == 0 )
                p_pvr->i_vid_std = V4L2_STD_NTSC;
            else if( strcmp( optarg, "SECAM" ) == 0 )
                p_pvr->i_vid_std = V4L2_STD_SECAM;
            else if( strcmp( optarg, "PAL" ) == 0 )
                p_pvr->i_vid_std = V4L2_STD_PAL;
            else
                msg_Warn( NULL, "'%s' is not a recognised video standard, auto-detecting.", optarg );
            break;
        case 'S':
            b_lsstds = 1;
            break;
        case 't':
            p_output->i_ttl = strtol( optarg, NULL, 10 );
            if( p_output->i_ttl < 1 )
            {
                msg_Warn( NULL, "TTL of %d is not valid, using default.", optarg );
                p_output->i_ttl = DEFAULT_TTL;
            }
            break;
        case 'u':
            p_output->i_flags |= OUTPUT_UDP;
            break;
        case 'V':
            exit( EXIT_SUCCESS );
            break;
        default:
            usage();
            exit( EXIT_FAILURE );
        }
    }

    // handle input list option before anything else

    if( b_lsinput )
    {
        if( !ivtv_Open() )
            ivtv_PrintInputList();
        ivtv_Close();
        exit( EXIT_SUCCESS );
    }

    if( b_lsstds )
    {
        if( !ivtv_Open() )
            if( !ivtv_SetInput() ) 
                ivtv_PrintStandards();
        ivtv_Close();
        exit( EXIT_SUCCESS );
    }

    // check IP address is actually specified

    if( optind >= i_argc )
    {
        usage();
        exit( EXIT_FAILURE );
    }

    // parse IP address and port number

    struct addrinfo *p_addr;
    struct addrinfo ai_hints;
    char *psz_ipaddr = p_argv[optind];
    char *psz_token;
    char sz_port[6];

    snprintf( sz_port, sizeof( sz_port ), "%d", DEFAULT_PORT );

    if( !strncmp( psz_ipaddr, "[", 1 ) )
    {
        // IPv6 address

        if( ( psz_token = strchr( psz_ipaddr, ']' ) ) == NULL )
        {
            msg_Err( NULL, "Invalid IPv6 address.  Aborting." );
            exit( EXIT_FAILURE );
        }

        char *psz_addr = malloc( psz_token - psz_ipaddr );
        memset( psz_addr, 0, ( psz_token - psz_ipaddr ) );
        strncpy( psz_addr, psz_ipaddr + 1, ( psz_token - psz_ipaddr - 1 ) );

        if( ( psz_token = strchr( psz_token, ':' ) ) != NULL )
        {
            *psz_token = 0;
            snprintf( sz_port, sizeof( sz_port ), "%d", atoi( psz_token + 1 ) );
        }

        p_addr = malloc( sizeof( p_addr ) );

        memset( &ai_hints, 0, sizeof( ai_hints ) );
        ai_hints.ai_socktype = SOCK_DGRAM;
        ai_hints.ai_flags    = AI_ADDRCONFIG | AI_NUMERICHOST | AI_NUMERICSERV;
        ai_hints.ai_family   = AF_INET6;

        int i_ai = getaddrinfo( psz_addr, sz_port, &ai_hints, &p_addr );

        if( i_ai != 0 )
        {
            free( psz_addr );
            msg_Err( NULL, "Cannot configure output: %s", gai_strerror( i_ai ) );
            exit( EXIT_FAILURE );
        }

        free( psz_addr );

        if( p_output->i_mtu == 0 )
            p_output->i_mtu = DEFAULT_MTU_IPV6;

        p_output->i_mtu -= 32;
    }

    else
    {
        // IPv4 address

        if( ( psz_token = strchr( psz_ipaddr, ':' ) ) != NULL )
        {
            *psz_token = 0;
            snprintf( sz_port, sizeof( sz_port ), "%d", atoi( psz_token + 1 ) );
        }

        p_addr = malloc( sizeof( p_addr ) );

        memset( &ai_hints, 0, sizeof( ai_hints ) );
        ai_hints.ai_socktype = SOCK_DGRAM;
        ai_hints.ai_flags    = AI_ADDRCONFIG | AI_NUMERICSERV;
        ai_hints.ai_family   = AF_INET;

        int i_ai = getaddrinfo( psz_ipaddr, sz_port, &ai_hints, &p_addr );

        if( i_ai != 0 )
        {
            msg_Err( NULL, "Cannot configure output: %s", gai_strerror( i_ai ) );
            exit( EXIT_FAILURE );
        }

        if( p_output->i_mtu == 0 )
            p_output->i_mtu = DEFAULT_MTU_IPV4;

        p_output->i_mtu -= 24;
    }

    // store IP address structure

    p_output->p_addr = malloc( p_addr->ai_addrlen );
    memcpy( p_output->p_addr, p_addr->ai_addr, p_addr->ai_addrlen );
    p_output->i_addrlen = p_addr->ai_addrlen;
    free( p_addr );

    // attempt to set real-time priority

    struct sched_param param;
    memset(&param, 0, sizeof( struct sched_param ));
    param.sched_priority = 1;

    int i_error;
    if( ( i_error = pthread_setschedparam( pthread_self(), SCHED_RR, &param ) ) )
    {
        msg_Dbg( NULL, "Failed to set real-time priority: %s", strerror( i_error ) );
        msg_Dbg( NULL, "If you are not running as root, you can probably ignore this." );
    }

    // open handles to PVR card and network socket

    if( ivtv_Open() )
        exit( EXIT_FAILURE );

    if( ivtv_InitEncoder() )
    {
        ivtv_Close();
        exit( EXIT_FAILURE );
    }

    if( output_Open() )
    {
        ivtv_Close();
        exit( EXIT_FAILURE );
    }

    // set up buffer and start streaming data

    output_Init();
    p_output->i_psdata_len = IVTV_READ_BUFFER;
    p_output->p_psdata = malloc( p_output->i_psdata_len );
    memset( p_output->p_psdata, 0, p_output->i_psdata_len );

    i_running = 1;

    tsmux_Run();

    output_Close();
    exit( EXIT_SUCCESS );
}
