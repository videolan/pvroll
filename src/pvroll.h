/*****************************************************************************
 * pvroll.h
 *****************************************************************************
 * Copyright (C) 2004,2006-2010 VideoLAN
 * $Id$
 *
 * Authors: Andy Gatward <a.j.gatward@reading.ac.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef __PVROLL_H_
#define __PVROLL_H_

#include <netdb.h>
#include <dvbpsi/dvbpsi.h>

/*****************************************************************************
 * System-level configuration
 *****************************************************************************/
#undef HAVE_CLOCK_NANOSLEEP

/*****************************************************************************
 * Application-wide defines and typedefs
 *****************************************************************************/
#define MIN_MTU             200     // Minimum MTU size (188 + 12)
#define MAX_MTU             9000    // Maximum MTU size
#define RTP_SIZE            12      // RTP header size
#define TS_SIZE             188     // Transport stream packet size
#define TS_PAYLOAD_SIZE     TS_SIZE - 4

#define DEFAULT_DEVICE      0       // default device (/dev/video0)
#define DEFAULT_INPUT       0       // default input on card
#define DEFAULT_PORT        5004    // default port number
#define DEFAULT_TTL         1       // default multicast TTL
#define DEFAULT_MTU_IPV4    1492    // default MTU for IPv4 transport
#define DEFAULT_MTU_IPV6    1280    // default MTU for IPv6 transport
#define DEFAULT_VB_AVG      3072    // default average video bandwidth
#define DEFAULT_VB_MAX      4096    // default maximum video bandwidth
#define DEFAULT_AB          224     // default audio bandwidth
#define DEFAULT_VERBOSE     1       // default verbosity set to warning level
#define DEFAULT_TSID        1001    // default MPEG TS ID
#define DEFAULT_SID         100     // default service ID
#define DEFAULT_PID_AUDIO   101     // default audio PID
#define DEFAULT_PID_VIDEO   102     // default video PID

#define IVTV_MAX_CTRLS      7       // maximum number of v4l2 ctrls to be set
#define IVTV_AUDIO_SAMPLE   48000   // audio sample rate (48kHz)
#define IVTV_READ_BUFFER    4096    // IVTV read buffer size

#define OUTPUT_UDP          0x01    // flag for UDP output mode

typedef int64_t mtime_t;

typedef struct block_t
{
    uint8_t p_ts[TS_SIZE];
} block_t;

typedef struct pvr_t
{
    int i_fd;
    uint8_t i_device;
    char sz_card[32];

    /* v4l2 params */
    int i_input;
    int i_freq;
    uint64_t i_vid_std;

    /* MPEG encoder params */
    int i_aspect;
    int i_audio_bitrate;
    int i_gop_size;
    int i_video_bitrate;
    int i_video_bitrate_peak;
    int i_video_cbr;
} pvr_t;

typedef struct output_t
{
    int i_fd;
    uint8_t i_flags;

    // network information
    struct sockaddr_storage *p_addr;
    socklen_t i_addrlen;
    uint16_t i_mtu;
    uint8_t i_ttl;

    // MPEG2-PS data storage
    unsigned char *p_psdata;
    uint32_t i_psdata_len;
    uint32_t i_psdata_max;

    // MPEG2-TS settings and state machine
    block_t *pp_blocks[MAX_MTU / TS_SIZE];
    uint8_t i_tsmux_state, i_pat_ver, i_pmt_ver, b_send_pcr;
    uint8_t i_cc_video, i_cc_audio, i_cc_pat, i_cc_pmt;
    uint16_t i_pid_video, i_pid_audio, i_pid_pmt, i_tsid, i_prognum;
    uint64_t i_pcr;
    dvbpsi_psi_section_t *p_pat_section, *p_pmt_section;

    // RTP handling
    uint32_t i_rtp_ssrc;
    uint16_t i_rtp_pos, i_rtp_depth;
    uint16_t i_rtp_seq;
    mtime_t i_rtp_tstamp, i_rtp_wclock;
} output_t;


/*****************************************************************************
 * Globals
 *****************************************************************************/
uint8_t i_verbose;      // console verbosity level
uint8_t i_running;      // running flag for main thread
pvr_t *p_pvr;           // PVR card configuration and handles
output_t *p_output;     // Output configuration and handles

/*****************************************************************************
 * Functions from ivtv.c
 *****************************************************************************/
int ivtv_Open();
void ivtv_Close();
int ivtv_Read( unsigned char *p_data, uint16_t i_len );
int ivtv_InitEncoder();
int ivtv_SetInput();
int ivtv_GetStandard( char *psz_standard );
int ivtv_PrintStandards();
int ivtv_SetStandard();
int ivtv_GetFrequency();
int ivtv_SetFrequency();
int ivtv_GetAudioBw();
int ivtv_SetAudioBw();
int ivtv_GetVideoAvgBw();
int ivtv_SetVideoAvgBw();
int ivtv_GetVideoPeakBw();
int ivtv_SetVideoPeakBw();
int ivtv_GetCbrMode();
int ivtv_SetCbrMode();
int ivtv_GetGopSize();
int ivtv_SetGopSize();
void ivtv_PrintInputList();
int ivtv_GetInput();
int ivtv_SetInput();
int ivtv_GetAspect();
int ivtv_SetAspect();

/*****************************************************************************
 * Functions from tsmux.c
 *****************************************************************************/
void tsmux_Run();

/*****************************************************************************
 * Functions from output.c
 *****************************************************************************/
void output_PutBlock( struct block_t *p_block );
int output_Open();
void output_Close();
void output_Init();

/*****************************************************************************
 * Functions from util.c
 *****************************************************************************/
void msg_Info( void *_unused, const char *psz_format, ... );
void msg_Err( void *_unused, const char *psz_format, ... );
void msg_Warn( void *_unused, const char *psz_format, ... );
void msg_Dbg( void *_unused, const char *psz_format, ... );
void msg_Raw( void *_unused, const char *psz_format, ... );
mtime_t mdate( void );
void msleep( mtime_t delay );

#endif
